﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Logic
{
    public class LanguageParser
    {
        private readonly List<string> _strings;

        public LanguageParser(string str)
        {
            _strings = str.RemovePunctuation().Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).ToList();

        }

        public override string ToString()
        {
            return string.Join(" ", GetAllWords().ToArray());
        }

        public List<string> GetAllWords()
        {
            return _strings;
        }

        public string PopWord()
        {
            var word = _strings.First();
            _strings.Remove(word);
            return word;
        }

        public string PeekWord()
        {
            return _strings.First();
        }


    }

    public static class StringExtensionMethods  //TODO: move to its own class
    {
        public static string RemovePunctuation(this string str)
        {
            return new string(str.Where(c => !char.IsPunctuation(c)).ToArray());
        }
    }
}
